# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <ctype.h>
# include <stdint.h>

unsigned char useless = 254;

typedef struct TagHuffmanNode
{
	unsigned char value;
	int16_t left;
	int16_t right;
} __attribute__((__packed__)) TagHuffmanNode;

typedef struct TreeNode {
	int binary;
	float probability;
	struct TreeNode *top;
	struct TreeNode *next;
	struct TreeNode *left_son;
	struct TreeNode *right_son;
	struct TagHuffmanNode *huffman;
} TreeNode;

	// Memorez pentru fiecare caracter lungimea codului asociat

typedef struct
{
	int code_size;
	unsigned char ch;
} Size;

void init(TreeNode **head)
{
	(*head) = NULL;
}

int isEmpty(TreeNode *head)
{
	if (head == NULL)
		return 1;
	return 0;
}

	// Functie care primeste caracterul si probabilitatea asociata si
	// creaza elementul de tip TreeNode ce urmeaza a fi inserat in
	// coada

TreeNode* tree_elem(unsigned char ch, float prob)
{
	TreeNode *new_element;
	new_element = (TreeNode*)malloc(sizeof(TreeNode));
	new_element->huffman = (TagHuffmanNode*)malloc(sizeof(TagHuffmanNode));

	new_element->huffman->value = ch;
	new_element->huffman->left = -1;
	new_element->huffman->right = -1;

	new_element->probability = prob;
	new_element->binary = 0;

	new_element->top = NULL;
	new_element->next = NULL;
	new_element->left_son = NULL;
	new_element->right_son = NULL;

	return new_element;
}

	// Inserez elemente astfel incat sa pastrez ordonarea crescatoare

void enqueue(TreeNode **head, TreeNode **new_elem)
{
	if (isEmpty(*head))
		(*head) = (*new_elem);

	else
	{
		if ((*head)->probability > (*new_elem)->probability)
		{
			(*new_elem)->next = (*head);
			(*head) = (*new_elem);
		}

		else
		{
			TreeNode *aux;
			TreeNode *temp = (*head);

			while (temp->next != NULL && temp->next->probability < (*new_elem)->probability)
				temp = temp->next;

			aux = temp->next;
			temp->next = (*new_elem);
			(*new_elem)->next = aux;
		}
	}
}

void dequeue(TreeNode **head)
{
	TreeNode *temp = (*head);

	(*head) = (*head)->next;
	temp->next = NULL;
}

	// Construiesc pe baza finally_binaryului de caractere citit un vector de
	// aparitii(occurrences) si un vector de probabilitati(prob)

void parse_string(unsigned char *text, int **occurrences, float **prob, int size)
{
	(*occurrences) = (int*)calloc(256, sizeof(int));
	(*prob) = (float*)malloc(256 * sizeof(float));

	int i, length = 0;

	for (i = 0; i < size; i++)
	{
		length++;
		(*occurrences)[(unsigned char)text[i]]++;
	}

	for (i = 0; i < 256; i++)
		(*prob)[i] = (float)(*occurrences)[i] / length;
}

	//Functie care creaza arborele Huffman conform celor mentionate in
	//enunt.

void create_huff(TreeNode **head, int *additional_nodes)
{
	static int i = 0;
	float sum;
	if ((*head)->next != NULL)
	{
		sum = (*head)->probability + (*head)->next->probability;
		{

			TreeNode *aux = tree_elem(useless, sum);

			aux->left_son = (*head);
			aux->left_son->binary = 0;
			aux->left_son->top = aux;
			dequeue(head);

			aux->right_son = (*head);
			aux->right_son->binary = 1;
			aux->right_son->top = aux;
			dequeue(head);

			(*additional_nodes)++;
			enqueue(head, &aux);
			(*head)->binary = 1;
			i++;
		}
	}
}

	// Functie care construieste recursiv codul pentru fiecare frunza sub forma
	// unei matrice liniarizate(folosesc un char *temp pentru a putea
	// lucra cu strcat). Ma folosesc de structura Size construita mai
	// sus, menita sa retina caracterul asociat si dimensiunea acestuia

void encode(TreeNode *root, unsigned char *bcode_array, TreeNode *aux, Size *sizes)
{
	static int i = 0;
	unsigned char *temp;

	if (root->left_son != NULL)
		encode(root->left_son, bcode_array, aux, sizes);

	if (root->right_son != NULL)
		encode(root->right_son, bcode_array, aux, sizes);

	if (root->left_son == NULL && root->right_son == NULL)
	{
		unsigned char leaf = root->huffman->value;
		temp = (unsigned char*)malloc(sizeof(unsigned char) * 2);
		sizes[i].code_size = 0;

		while(root != aux)
		{
			temp[0] = (unsigned char)(((root->binary) % 2) + '0');
			temp[1] = '\0';
			strcat (bcode_array, temp);
			aux->left_son->next = NULL;
			root = root->top;
			sizes[i].code_size++;
		}

		free(temp);
		sizes[i].ch = leaf;
		i++;
	}
}

	// Functie care afiseaza codul intr-un fisier din care urmeaza sa
	// citesc cu ajutorul lui fread intr-un finally_binary de caractere numit text

void printcode(unsigned char *bcode_array, Size *elems, unsigned char tofind, FILE *temp_out)
{
	int i = 0, j = 0, k;

	while (tofind != elems[i].ch)
	{
		j += elems[i].code_size;
		i++;
	}

	k = j;
	j += elems[i].code_size;

	for(j = j - 1 ; j >= k; j--)
		fprintf(temp_out, "%c", bcode_array[j]);
}

	// Functie ce creaza in mod recursiv vectorul de structuri
	// TagHuffmanNode ce urmeaza a fi scris in fisier

int place_node(TagHuffmanNode **vector, TreeNode *root, int *index)
{
	TreeNode *nod;
	int current;

	current = ++(*index);
	vector[current] = root->huffman;

	if (root->left_son != NULL)
		root->huffman->left = place_node(vector, root->left_son, index);

	if (root->right_son != NULL)
		root->huffman->right = place_node(vector, root->right_son, index);

	return current;
}

	// Functie ce reconstruieste in mod recursiv arborele cu ajutorul
	// vectorului de structuri citit din fisier

void remake_tree(TagHuffmanNode **ve, TreeNode **root, int i)
{
	if (*root == NULL)
		(*root) = (TreeNode *)malloc(sizeof(TreeNode));

	(*root)->huffman = ve[i];

	if ((*root)->huffman->left != -1)
	{
		(*root)->left_son = (TreeNode*)malloc(sizeof(TreeNode));
		remake_tree(ve, &((*root)->left_son), (*root)->huffman->left);
	}
	else
		(*root)->left_son = NULL;

	if ((*root)->huffman->right != -1)
	{
		(*root)->right_son = (TreeNode*)malloc(sizeof(TreeNode));
		remake_tree(ve, &((*root)->right_son), (*root)->huffman->right);
	}
	else
		(*root)->right_son = NULL;
}

	// Eliberez recursiv memoria ocupata de arbore

void free_tree(TreeNode *nod)
{
	if (nod == NULL)
		return;

	if (nod->left_son != NULL)
		free_tree(nod->left_son);

	if (nod->right_son != NULL)
		free_tree(nod->right_son);

	free(nod->huffman);
	free(nod);
}

unsigned char *strrev(unsigned char *string)
{
	unsigned char *left, *right;
	if (! string || ! *string)
		return string;
	for (left = string, right = string + strlen(string) - 1; right > left; ++left, --right)
	{
		*left ^= *right;
		*right ^= *left;
		*left ^= *right;
	}
	return string;
}

	// Primind ca parametru vectorul cu bytesii corespunzatori codului,
	// vreau sa obtin un finally_binary de caractere de 0 si 1

char *bin2str(unsigned char *bcode_array, int nr_bytes)
{
	int i, k;
	unsigned char *code = (unsigned char*)malloc(8 * nr_bytes * sizeof(unsigned char));
	for (i = 0; i < nr_bytes; i++)
	{
		int j = 0;
		unsigned char aux[8];

		while (bcode_array[i])
		{
			aux[j] = bcode_array[i] % 2 + '0';
			bcode_array[i] = bcode_array[i] / 2;
			j++;
		}

		for (; j < 8; j++)
			aux[j] = 48;

		char *s = strrev(aux);

		for (k = 0; k < 8; k++)
		{
			s[k] -= '0';
			code[8 * i + k] = s[k];
		}
	}
	return code;
}

	// Folosind finally_binaryul de caractere obtinut cu bin2str, decodific
	// caracterele

void get_text(TreeNode *head, unsigned char *bcode_array, int nr_bytes, FILE* out)
{
	int i;
	TreeNode *temp = head;

	for (i = 0; i <= nr_bytes * 8; i++)
	{
		if (bcode_array[i] == 1)
		{
			if (temp->right_son != NULL)
				temp = temp->right_son;
			else
			{
				fprintf(out, "%c", temp->huffman->value);
				temp = head;
				i--;
			}
		}

		else if (bcode_array[i] == 0)
		{
			if (temp->left_son != NULL)
				temp = temp->left_son;
			else
			{
				fprintf(out, "%c", temp->huffman->value);
				temp = head;
				i--;
			}
		}
	}

}

int main(int argc, unsigned char *argv[])
{
	if (strcmp(argv[1],"-c") == 0)
	{
		int i;
		FILE *stream_reader = fopen(argv[2], "r");
		FILE *temp_out = fopen("temporar.out", "w+");
		FILE *out = fopen(argv[3], "w+b");

		fseek(stream_reader, 0L, SEEK_END);
		int size = ftell(stream_reader);
		fseek(stream_reader, 0L, SEEK_SET);

		// Scriu in fisierul binar numarul de caractere din finally_binaryul citit
		uint32_t charnumber = size;
		fwrite(&charnumber, sizeof(uint32_t), 1, out);

		unsigned char *text = (unsigned char*)malloc(sizeof(unsigned char) * (size + 1));
		fread(text, sizeof(unsigned char), size,  stream_reader);
		text[size] = '\0';

		int *occurrences;
		float *prob;

		parse_string(text, &occurrences, &prob, size);
		TreeNode *head;
		init(&head);

		int maxiter = 0;

		// Creez in cazul in care occurrences[i] este nenul (caracterul
		// ascii corespunztor are probabilitatea diferita de 0) un nod
		// ce urmeaza a fi inserat in coada
		for (i = 0; i < 256; i++)
			if (occurrences[i])
			{
				unsigned char character = i;
				maxiter++;
				TreeNode *temp = tree_elem(character, prob[i]);
				enqueue(&head, &temp);
			}

		free(occurrences);
		free(prob);

		int uniques = maxiter;
		int additional_nodes = 0;

		// Numarul de iteratii va fi numarul de caractere unice
		// existente - 1. Pentru fiecare astfel de caracter, creez un
		// nou nod in arbore
		maxiter--;
		while(maxiter)
		{
			create_huff(&head, &additional_nodes);
			maxiter--;
		}

		// Scriu in fisier numarul de noduri ale arborelui calculat ca
		// fiind numarul de caractere unice + numarul de noduri nou
		// create dupa algoritmul de generare al arborelui

		uint16_t nr_nodes = uniques + additional_nodes;
		fwrite(&nr_nodes, sizeof(uint16_t), 1, out);

		TagHuffmanNode **vector = (TagHuffmanNode**)malloc(nr_nodes * sizeof(TagHuffmanNode*));
		int index = -1;
		place_node(vector, head, &index);

		for (i = 0; i < nr_nodes; i++) 
			fwrite(vector[i], sizeof(TagHuffmanNode), 1, out);

		free(vector);

		// Codific mesajul cu ajutorul structurii Size
		Size *sizes;
		sizes = (Size*)malloc((uniques + 1) * sizeof(Size));

		unsigned char bcode_array[256 * 16];
		bcode_array[0] = '\0';

		encode(head, bcode_array, head, sizes);

		// Scriu codificarea cu 0 si 1 in fisierul auxiliar
		for (i = 0; i < size; i++)
			printcode(bcode_array, sizes, text[i], temp_out);

		free(text);
		free(sizes);

		// Urmeaza sa citesc cu fread in text codul din fisierul
		// auxiliar
		fseek(temp_out, 0, SEEK_END);
		int asize = ftell(temp_out);
		fseek(temp_out, 0, SEEK_SET);
		unsigned char *codes = (unsigned char*)malloc(sizeof(unsigned char) * (asize + 1));

		fread(codes, sizeof(unsigned char), asize, temp_out);
		codes[asize] = 0;

		unsigned char finally_binary = 0;
		int count = -1, ok = 0;

		// asize = dimensiunea fisierului ce contine doar codul
		// (lungimea acestuia)

		// Scriu cate 8 biti in fisierul binar
		for (i = 0; i < asize; i++)
		{
			if (++count == 8)
			{
				fwrite(&finally_binary, sizeof(unsigned char), 1, out);
				finally_binary = count = ok = 0;
			}

			if(codes[i] == '1')
			{
				finally_binary <<= 1;
				finally_binary |= 1;
				ok = 1;
			}
			else
			{
				finally_binary <<= 1;
				ok = 1;
			}

		}
		free(codes);

		// Daca este necesar, fac padding pana la 1 byte
		for (count = count + 1; ok && count != 8; count++)
			finally_binary <<= 1;

		if (ok != 0)
			fwrite(&finally_binary, sizeof(unsigned char), 1, out);

		free_tree(head);


		fclose(stream_reader);
		fclose(out);
		fclose(temp_out);
	}

		// DECOMPRESIE
	if (strcmp(argv[1], "-d") == 0)
	{
		FILE *stream_reader = fopen(argv[2], "rb");
		FILE *out = fopen(argv[3], "w");

		// Citesc din fisierul binar numarul de caractere
		fseek(stream_reader, 0, SEEK_SET);
		uint32_t charnumber;
		fread(&charnumber, sizeof(uint32_t), 1, stream_reader);

		// Citesc din fisierul binar numarul de noduri
		uint16_t nr_nodes;
		fseek(stream_reader, 4, SEEK_SET);
		fread(&nr_nodes, sizeof(uint16_t), 1, stream_reader);

		// Citesc vectorul de noduri din fisierul binar
		TagHuffmanNode **ve = (TagHuffmanNode**)malloc(nr_nodes * sizeof(TagHuffmanNode*));
		fseek(stream_reader, 6, SEEK_SET);
		int i;

		for (i = 0; i < nr_nodes; i++)
		{
			ve[i] = (TagHuffmanNode*)malloc(sizeof(TagHuffmanNode));
			fread(ve[i], sizeof(TagHuffmanNode), 1, stream_reader);
		}

		// Ma pozitionez inaintea codului din binar si aflu dimensiunea
		// acestuia ca numar de bytes
		fseek(stream_reader, 6 + nr_nodes * sizeof(TagHuffmanNode), SEEK_SET);
		int begin = ftell(stream_reader);

		fseek(stream_reader, 0, SEEK_END);
		int end = ftell(stream_reader);
		int bcode_size = end - begin;

		unsigned char *bcode_array = (unsigned char*)malloc(bcode_size * sizeof(unsigned char));

		// Citesc codul rezultat in urma compresiei din fisierul binar
		fseek(stream_reader, 6 + nr_nodes * sizeof(TagHuffmanNode), SEEK_SET);
		fread(bcode_array, sizeof(unsigned char), bcode_size, stream_reader);

		fclose(stream_reader);

		// Recreez arborele
		TreeNode *head;
		init(&head);
		remake_tree(ve, &head, 0);

		// Obtin un finally_binary de caractere de dimensiune bcode_size * 8
		// cu ajutorul caruia decodific textul
		char *finally_binary = bin2str(bcode_array, bcode_size);

		free(bcode_array);

		get_text(head, finally_binary, bcode_size, out);

		free(finally_binary);
		free_tree(head);
		free(ve);
		fclose(out);
	}

	return 0;
}
