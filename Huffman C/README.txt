My C implementation of the Huffman algorithm involving compression as well as decompression.


make

./huffman -c sourceFile destinationFile
./huffman -d destinationFile decompressionFile

diff sourceFile decompressionFile to see they are identical

I also have two test files : 
	- alAscii(containing all ascii characters for removing any doubt of failure)
	- Kubrick (a normal text file)