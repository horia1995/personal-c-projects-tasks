This is a Hard Disk Simulation using linked lists (circular and double-linked)
The idea would be that the HDD is composed of a double linked-list of 10 circular linked lists.

Given a time(number of steps), I had to be able to iterate through the nodes (the HDD zones)
in order to read a given sequence, write a given sequence or read the damage done to a certain zone.

The damaging process is the following:
Each operation damages the respective zone in three cases:
	- simply iterating through the zone
	- reading from the zone
	- writing to the zone

When the time is up, the HDD cursor stops and remains in that position until it is given another task.
When this happens, we have more time, but we use it to execute the commands in a QUEUE manner or a STACK manner, so we do not just give up on the task if the time is up, but instead we use it to do the unfinished previous commands.

This was a homework during the Data Structures course I attended,
so the only remaining test files are these.

make
./myHDD data.in data.out

See the result.

