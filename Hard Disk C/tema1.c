#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Command
{
	int linie, index, time;
	char *com_name, *data;
	struct Command *next;
} Command;

typedef struct CircList
{
	char *data;
	struct CircList *next;
	int damage;
} CircList;

typedef struct DoubleList
{
	struct DoubleList *up;
	struct DoubleList *down;
	struct CircList *list;
	int indice;
} DoubleList;

typedef struct Iterator
{
	struct DoubleList *lista_curenta;
	struct CircList *elem_curent;
	int pos_circulara;
} Iterator;

int isEmptyDoub(DoubleList *list)
{
	if (list == NULL)
		return 1;
	return 0;
}

int isEmptyCirc(CircList *head)
{
	if (head == NULL)
		return 1;
	return 0;
}

int isEmptyQS(Command *first)
{
	if (first == NULL)
		return 1;
	return 0;
}

void PushQueue(Command **first, FILE *f)
{
	char buf[256];
	if (isEmptyQS(*first))
	{
		(*first) = (Command*)malloc(sizeof(Command));
		(*first)->com_name = (char*)malloc(6 * sizeof(char));
		(*first)->linie = (*first)->index = 0;
		(*first)->data = NULL;

		fscanf(f, "%s", ((*first)->com_name));
		if (strcmp((*first)->com_name, "::e") != 0)
		{
			fscanf(f, "%d", &((*first)->linie));
			fscanf(f, "%d", &((*first)->index));
			if (strcmp((*first)->com_name, "::w") == 0)
			{
				(*first)->data = (char*)malloc(5 * sizeof(char));
				fscanf(f, "%s", ((*first)->data));
			}
			if (strcmp((*first)->com_name, "::mw") == 0)
			{
				fgets(buf, 255, f);
			}

			fscanf(f, "%d", &((*first)->time));
		}
		(*first)->next = NULL;
	}

	else
	{
		Command *temp;
		temp = (Command*)malloc(sizeof(Command));
		temp->com_name = (char*)malloc(6 * sizeof(char));
		temp->linie = temp->index = 0;
		temp->data = NULL;

		fscanf(f, "%s", temp->com_name);
		if (strcmp(temp->com_name, "::e") != 0)
		{
			fscanf(f, "%d", &(temp->linie));
			fscanf(f, "%d", &(temp->index));
			if (strcmp(temp->com_name, "::w") == 0)
			{
				temp->data = (char*)malloc(5 * sizeof(char));
				fscanf(f, "%s", temp->data);
			}
			if (strcmp(temp->com_name, "::mw") == 0)
			{
				fgets(buf, 255, f);
			}

			fscanf(f, "%d", &(temp->time));
		}
		temp->next = (*first);
		(*first) = temp;
	}
}

void PushStack(Command **top, FILE *f)
{
	char buf[256];

	if (isEmptyQS(*top))
	{
		(*top) = (Command*)malloc(sizeof(Command));
		(*top)->com_name = (char*)malloc(6 * sizeof(char));
		(*top)->linie = 0;
		(*top)->index = 0;
		(*top)->data = NULL;

		fscanf(f, "%s", ((*top)->com_name));
		if (strcmp((*top)->com_name, "::e") != 0)
		{
			fscanf(f, "%d", &((*top)->linie));
			fscanf(f, "%d", &((*top)->index));
			if (strcmp((*top)->com_name, "::w") == 0)
			{
				(*top)->data = (char*)malloc(5 * sizeof(char));
				fscanf(f, "%s", ((*top)->data));
			}
			if (strcmp((*top)->com_name, "::mw") == 0)
			{
				fgets(buf, 255, f);
			}
			fscanf(f, "%d", &((*top)->time));
		}
		(*top)->next = NULL;
	}
	else
	{
		Command *temp;
		temp = (Command*)malloc(sizeof(Command));
		temp->com_name = (char*)malloc(6 * sizeof(char));
		temp->linie = 0;
		temp->index = 0;
		temp->data = NULL;

		fscanf(f, "%s", temp->com_name);
		if (strcmp(temp->com_name, "::e") != 0)
		{
			fscanf(f, "%d", &(temp->linie));
			fscanf(f, "%d", &(temp->index));
			if (strcmp(temp->com_name, "::w") == 0)
			{
				temp->data = (char*)malloc(5 * sizeof(char));
				fscanf(f, "%s", temp->data);
			}
			if (strcmp(temp->com_name, "::mw") == 0)
			{
				fgets(buf, 255, f);
			}
			fscanf(f, "%d", &(temp->time));
		}
		temp->next = (*top);
		(*top) = temp;
	}
}

void PopQueue(Command **first)
{
	Command *temp = (*first);
	if ((*first)->next == NULL) {
		free(*first);
		*first = NULL;
	}

	else
	{
		while (temp->next->next != NULL)
			temp = temp->next;

		free(temp->next);
		temp->next = NULL;
	}
}

void PopStack(Command **top)
{
	if ((*top)->next != NULL)
	{
		Command *temp = (*top);
		(*top) = (*top)->next;
		free(temp);
	}

	else {
		free(*top);
		*top = NULL;
	}
}

void addDoub(DoubleList **list, CircList *circ)
{
	if (isEmptyDoub(*list))
	{
		(*list) = (DoubleList*)malloc(sizeof(DoubleList));
		(*list)->up = NULL;
		(*list)->down = NULL;
		(*list)->list = circ;
	}

	else
	{
		DoubleList *temp = (*list);
		DoubleList *new;

		while (temp->up != NULL)
			temp = temp->up;

		new = (DoubleList*)malloc(sizeof(DoubleList));
		temp->up = new;
		new->down = temp;
		new->up = NULL;
		new->list = circ;
	}
}

void addBackCirc(CircList **head)
{
	if (isEmptyCirc(*head))
	{
			(*head) = (CircList*)malloc(sizeof(CircList));
			(*head)->next = (*head);
			(*head)->data = "0000";
			(*head)->damage = 0;
	}

	else
	{
		CircList *temp = (*head);

		while (temp->next != (*head))
			temp = temp->next;

		temp->next = (CircList*)malloc(sizeof(CircList));
		temp->next->next = (*head);
		temp->next->data = "0000";
		temp->next->damage = 0;
	}
}

void print(CircList* head)
{
	CircList *temp = head;

	while (temp->next != head)
	{
		printf("%s ", temp->data);
		temp = temp->next;
	}
	printf("%s ", temp->data);
	printf("\n");
}

void move(int linie, int index, Iterator *it, int *totaltime)
{
	int i;
	DoubleList *temp = it->lista_curenta;
	CircList *aux = it->elem_curent;

	if (linie != temp->indice)
	{
		while (it->pos_circulara != 0)
		{
			if (!totaltime)
				break;
			aux = aux->next;
			aux->damage++;
			totaltime--;
			it->pos_circulara++;

			if (aux == it->lista_curenta->list)
				it->pos_circulara = 0;
		}

		if (linie < temp->indice)
			for (i = 0; i < it->lista_curenta->indice - linie; ++i)
			{
				if (!totaltime)
					break;
				temp = temp->down;
				temp->list->damage++;
				totaltime--;
			}

		else
			for (i = 0; i < linie - it->lista_curenta->indice; i++)
			{
				if (!totaltime)
					break;
				temp = temp->up;
				temp->list->damage++;
				totaltime--;
			}

		it->lista_curenta = temp;
		it->elem_curent = it->lista_curenta->list;

		aux = it->lista_curenta->list;
	}


	while (it->pos_circulara != index)
	{
		if (!totaltime)
			break;
		aux = aux->next;
		aux->damage++;
		totaltime--;
		it->pos_circulara++;
		if (aux == it->lista_curenta->list)
			it->pos_circulara = 0;

		it->elem_curent = aux;
	}
}
void r(int linie, int index, Iterator *it, int *totaltime, FILE *out)
{
	move(linie, index, it, totaltime);
	fprintf(out, "%s\n", it->elem_curent->data);
	it->elem_curent->damage += 5;
}

void w(int linie, int index, char *value, Iterator *it, int *totaltime)
{
	move(linie, index, it, totaltime);
	it->elem_curent->data = value;
	it->elem_curent->damage += 30;
}

void d(int linie, int index, Iterator *it, int *totaltime, FILE *out)
{
	move(linie, index, it, totaltime);
	fprintf(out, "%d\n", it->elem_curent->damage);
	it->elem_curent->damage += 2;
}

int main(int argc, char *argv[])
{
	FILE *f = fopen(argv[1], "r");
	FILE *out = fopen(argv[2], "w");

	int i, j, totaltime = 0;
	int queueOrStack, disksNumber;

	CircList *head = NULL;
	DoubleList *list = NULL, *temp;
	Command *first = NULL;
	Iterator it;

	for (i = 0; i < 10; ++i)
	{
		head = NULL;
		addBackCirc(&head);
		for (j = 0; j < (1 << (4 + i)) - 1; ++j)
			addBackCirc(&head);
		addDoub(&list, head);
	}

	temp = list;
	for (i = 0; i < 10; ++i)
	{
		temp->indice = i;
		temp = temp->up;
	}

	it.lista_curenta = list;
	it.elem_curent = list->list;
	it.pos_circulara = 0;

	fscanf(f, "%d", &queueOrStack);
	fscanf(f, "%d", &disksNumber);

	if (queueOrStack == 1)
		PushQueue(&first, f);
	else
	{
		if (queueOrStack == 2)
			PushStack(&first, f);
	}

	totaltime += first->time;
	while (strcmp(first->com_name, "::e"))
	{
		while (totaltime >= 0)
		{
			if (strcmp(first->com_name, "::r") == 0)
			{
				r(first->linie, first->index, &it, &totaltime, out);
				it.elem_curent->damage++;
			}

			else if (strcmp(first->com_name, "::w") == 0)
			{
				w(first->linie, first->index, first->data, &it, &totaltime);
				it.elem_curent->damage++;
			}

			else if (strcmp(first->com_name, "::d") == 0)
			{
				d(first->linie, first->index, &it, &totaltime, out);
				it.elem_curent->damage++;
			}
			
			if (first->next == NULL)
			{
				while (totaltime >= 0)
				{
					it.elem_curent->damage++;
					totaltime--;
				}
				if (queueOrStack == 1)
					PopQueue(&first);
				else
				{
					if (queueOrStack == 2)
						PopStack(&first);
				}
			}

			else
			{
				if (queueOrStack == 1)
					PopQueue(&first);
				else
				{
					if (queueOrStack == 2)
						PopStack(&first);
				}
			}
		}

		if (queueOrStack == 1)
			PushQueue(&first, f);
		else
		{
			if (queueOrStack == 2)
				PushStack(&first, f);
		}
		if (strcmp(first->com_name, "::e") != 0)
			totaltime += first->time;
	}


	fclose(f);
	fclose(out);
	return 0;
}
