/* Programarea Calculatoarelor, seria CC
 * Tema2 - Snake
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <curses.h>
#include <ctype.h>
#include <sys/select.h>

#define MIN_ROWS 	10
#define MIN_COLS 	10
#define MAX_ROWS 	35
#define MAX_COLS 	80
#define INIT_ROW 	15
#define INIT_COL 	40
#define INIT_LENGTH  7
#define MAX_LENGTH  60
#define NO_OBST 	10
#define UP	8
#define DO	2
#define LE	4
#define RI	6

typedef struct
{
	int x;
	int y;
}punct;

void nume()
{
	mvaddstr(1,12,"    oo_   \\\\\\  ///              _      ");
	mvaddstr(2,12,"  /  _)-<((O)(O))   /)  (OO) .' )wWw   ");
	mvaddstr(3,12,"  \\__ `.  | \\ ||  (o)(O) ||_/ .' (O)_  ");
	mvaddstr(4,12,"     `. | ||\\\\||   //\\\\  |   /  .' __) ");
	mvaddstr(5,12,"     _| | || \\ |  |(__)| ||\\ \\ (  _)   ");
	mvaddstr(6,12,"  ,-'   | ||  ||  /,-. |(/\\)\\ `.`.__)  ");
	mvaddstr(7,12," (_..--' (_/  \\_)-'   ''     `._)      ");
}

void colors()
{
	start_color();
	init_pair(1, COLOR_GREEN, COLOR_BLACK);
	init_pair(2, COLOR_RED, COLOR_BLACK);
	init_pair(3, COLOR_GREEN, COLOR_BLACK);
	bkgd(COLOR_PAIR(3));
}

void startgame()
{
	noecho();
	cbreak();
	curs_set(0);
	refresh();
	srand(time(NULL));
}

void createsnake(int row, int col, punct snake[], int slength)
{
	int i;
	for (i = 0; i < INIT_LENGTH; ++i)
	{
		snake[i].x = row;
		snake[i].y = col - i;
	}
}

void head(punct snake[], int row, int col)
{
	snake[0].x = row;
	snake[0].y = col;
}

void removetail(punct snake[], int slength)
{
	mvaddch(snake[slength - 1].x, snake[slength - 1].y , ' ');
	snake[slength - 1].x = 0;
	snake[slength - 1].y = 0;
}

void movesn(punct snake[], int slength)
{
	int i;
	for (i = slength; i > 0; --i)
		snake[i] = snake[i - 1];
}

void buildsn(punct snake[], int slength, punct food)
{
	int i;
	for (i = 0; i < slength; ++i)
		if (i == 0)
		{
			attron(COLOR_PAIR(3));
			mvaddch(snake[i].x, snake[i].y, '@');
			attroff(COLOR_PAIR(3));
		}
		else
			if (snake[i].x != 0 && snake[i].y != 0)
			{
				attron(COLOR_PAIR(3));
				mvaddch(snake[i].x, snake[i].y, 'o');
				attroff(COLOR_PAIR(3));
			}
	attron(COLOR_PAIR(1));
	mvaddch(food.x, food.y, '*');
	attroff(COLOR_PAIR(1));
	refresh();
}

void grow(punct snake[], int slength)
{
	snake[slength].x = snake[slength - 1].x;
	snake[slength].y = snake[slength - 1].y;
}

int not_occupied(punct whatever, punct snake[], int slength)
{
	int i, ok = 0;
	for (i = 0; i < slength; ++i)
	{
		if (snake[i].x == whatever.x && snake[i].y == whatever.y)
		{
			ok = 1;
			break;
		}
	}
	return ok;
}

int randomse(int start, int end)
{
	int nr = 0, lownr, highnr;
	if (start < end)
	{
		lownr = start;
		highnr = end;
	}
	else
	{
		lownr = end;
		highnr = start;
	}
	nr = (rand() % (highnr - lownr)) + lownr;
	return nr;
}

void placefood(punct *food, punct snake[], int slength, punct obstacle[], int nobst, int nrows, int ncols)
{
	do
	{
		food->x = randomse(11, nrows);
		food->y = randomse(11, ncols);
	} while (not_occupied(*food, snake, slength) || not_occupied(*food, obstacle, nobst));
}

void placeobstacle(punct *obstacle, punct snake[], int slength, int nobst, int nrows, int ncols)
{
	int i = 0;
	do
	{
		for (; i < nobst; ++i)
		{
			obstacle[i].x = randomse(11, nrows);
			obstacle[i].y = randomse(11, ncols);
		}
	} while (not_occupied(*obstacle, snake, slength));
}

int hitself(punct snake[], int slength)
{
	int ok = 0, i;
	for (i = 1; i < slength; ++i)
		if (snake[0].x == snake[i].x && snake[0].y == snake[i].y)
			ok = 1;
	return ok;
}

int hitobstacle(punct snake[], punct obstacle[], int nobst)
{
	int ok = 0, i;
	for (i = 0; i < nobst; ++i)
		if (snake[0].x == obstacle[i].x && snake[0].y == obstacle[i].y)
			ok = 1;
	return ok;
}

int main(void)
{
	struct timespec timeout;
	timeout.tv_sec = 0;
	timeout.tv_nsec = 120000000;

	int row = INIT_ROW, col = INIT_COL, new_row, new_col;
	int slength = INIT_LENGTH, nobst = NO_OBST;
	int i, dir = RI, FOREVER = 1;
	int irows = MIN_ROWS, icols = MIN_COLS, nrows = MAX_ROWS, ncols = MAX_COLS;
	int ok = 0, score = 0, obs = 0;

	char c, d;

	punct snake[MAX_LENGTH];
	punct food;
	punct obstacle[NO_OBST];

	WINDOW *wnd;
	initscr();
	wnd = newwin(nrows - irows + 1, ncols - icols + 1, irows, icols);
	nodelay(wnd, TRUE);

	// write menu options
	mvaddstr(2, 60, "PRESS 'O' TO CHOOSE OBSTACLE MODE");
	mvaddstr(4, 60, "PRESS 'N' TO CHOOSE NO-OBSTACLE MODE");
	mvaddstr(6, 60, "PRESS 'Q' TO QUIT");

	colors();
	nume();

	startgame();
	box(wnd, 0, 0); // draw window borders

	createsnake(row, col, snake, slength);

	// create menu options
	do
	{
		d = wgetch(wnd);
		if (d == 'o')
			obs = 1;
		else
			if (d == 'n')
				obs = 0;
			else
				if (d == 'q')
					{
						clear();
						endwin();
						return 0;
					}
				else
					d = ERR;
	} while (d == ERR);

	// clear menu options
	mvaddstr(2, 60, "                                 ");
	mvaddstr(4, 60, "                                    ");
	mvaddstr(6, 60, "                 ");
	mvaddstr(5, 70, "SCORE = 0");

	if(obs == 1)
		placeobstacle(obstacle, snake, slength, nobst, nrows, ncols);
	placefood(&food, snake, slength, obstacle, nobst, nrows, ncols);

	while (FOREVER)
	{
		c = wgetch(wnd);
		switch (tolower(c))
		{
			case 'a':
				if (dir == UP || dir == DO)
				dir = LE;
				break;
			case 'd':
				if (dir == UP || dir == DO)
				dir = RI;
				break;
			case 'w':
				if (dir == LE || dir == RI)
				dir = UP;
				break;
			case 's':
				if (dir == LE || dir == RI)
				dir = DO;
				break;
			case 'q':
				FOREVER = 0;
				break;
		}

		switch (dir)
		{
			case LE:
				new_row = row;
					new_col = col - 1;
				break;
			case RI:
				new_row = row;
					new_col = col + 1;
				break;
			case UP:
				new_col = col;
					new_row = row - 1;
				break;
			case DO:
				new_col = col;
					new_row = row + 1;
				break;
		}
		row = new_row;
		col = new_col;

		// snake eats
		if (row == food.x && col == food.y)
		{
			grow(snake, slength);
			slength++;
			placefood(&food, snake, slength, obstacle, nobst, nrows, ncols);
			timeout.tv_nsec = timeout.tv_nsec - timeout.tv_nsec/20; // waits less so speed increases
			score = score + 10;

			// print score
			mvaddstr(5, 70,"SCORE =");
			mvprintw(5, 78, "%d", score);

			// WIN GAME (probably will not happen)
			if (score == 500)
			{
				mvaddstr(19, 25, "             CONGRATIULATIONS!");
				mvaddstr(20, 25, "                  YOU WON !");
				mvaddstr(22, 25, "            PLEASE, BE PATIENT!");
				mvaddstr(23, 15, "YOU WILL EVENTUALLY GET YOUR TERMINAL BACK! ENJOY THE VICTORY!");
				ok = 1;
				refresh();
			}
			if (ok == 1) // you won
			{
				timeout.tv_sec = 3;
				nanosleep(&timeout, NULL);
			}
		}
		else removetail(snake, slength);

		// hit margins, self or obstacle
			// hit margins
		if (row <= irows || row >= nrows || col <= icols || col >= ncols)
			{
				mvaddstr(19, 25, "                GAME OVER!");
				mvaddstr(20, 25, "         THE ROOM AIN'T BIG ENOUGH!");
				mvaddstr(22, 25, "            PLEASE, BE PATIENT!");
				mvaddstr(23, 25, "YOU WILL GET YOUR TERMINAL BACK IN 3 SECONDS!");
				ok = 1;
				refresh();
			}
			if (ok == 1) // you hit one of the margins
			{
				timeout.tv_sec = 5;
				nanosleep(&timeout, NULL);
				break;
			}

		if (hitself(snake, slength))
			{
				mvaddstr(19, 25,   "               GAME OVER!");
				mvaddstr(20, 25, "      YOU HAVE JUST COMMITED SUICIDE!");
				mvaddstr(22, 25, "            PLEASE, BE PATIENT!");
				mvaddstr(23, 25, "YOU WILL GET YOUR TERMINAL BACK IN 3 SECONDS!");
				refresh();
				timeout.tv_sec = 3;
				nanosleep(&timeout, NULL);
				break;
			}

		if (hitobstacle(snake, obstacle, nobst))
			{
				mvaddstr(19, 25, "                 GAME OVER!");
				mvaddstr(20, 25, "            YOUR 'X' KILLED YOU!");
				mvaddstr(22, 25, "            PLEASE, BE PATIENT!");
				mvaddstr(23, 25, "YOU WILL GET YOUR TERMINAL BACK IN 3 SECONDS!");
				refresh();
				timeout.tv_sec = 3;
				nanosleep(&timeout, NULL);
				break;
			}

		// marks obstacle
		if(obs == 1)
			for (i = 0; i < nobst; ++i)
			{
				attron(COLOR_PAIR(2));
				mvaddch(obstacle[i].x, obstacle[i].y, 'X');
				attroff(COLOR_PAIR(2));
			}

		movesn(snake, slength);
		head(snake, row, col);
		buildsn(snake, slength, food);
		refresh();
		nanosleep(&timeout, NULL);
	}

	clear();
	endwin();
	return 0;
}
